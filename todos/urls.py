from django.urls import path
from todos.views import (
    TodoListView,
    TodoListDetailView,
    create_TodoList,
    update_TodoList,
    delete_TodoList,
    create_TodoItem,
    update_TodoItem,
)

urlpatterns = [
    path("", TodoListView, name="todo_list_list"),
    path("<int:id>/", TodoListDetailView, name="todo_list_detail"),
    path("create/", create_TodoList, name="todo_list_create"),
    path("<int:id>/edit/", update_TodoList, name="todo_list_update"),
    path("<int:id>/delete/", delete_TodoList, name="todo_list_delete"),
    path("items/create/", create_TodoItem, name="todo_item_create"),
    path("items/<int:id>/edit/", update_TodoItem, name="todo_item_update"),
]
