from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


# List View
def TodoListView(request):
    TodoList_list = TodoList.objects.all()
    context = {"todolist": TodoList_list}
    return render(request, "todos/list.html", context)


# Detail View
def TodoListDetailView(request, id):
    list_details = TodoList.objects.get(id=id)
    context = {
        "list_details": list_details,
    }
    return render(request, "todos/details.html", context)


# Create Lists
def create_TodoList(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            TodoList = form.save()
            return redirect("todo_list_detail", id=TodoList.id)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


# Edit Lists
def update_TodoList(request, id):
    TodoList_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=TodoList_list)
        if form.is_valid():
            TodoList_list = form.save()
            return redirect("todo_list_detail", id=TodoList_list.id)

    else:
        form = TodoListForm(instance=TodoList_list)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


# Delete Lists
def delete_TodoList(request, id):
    TodoList_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        TodoList_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


# Feature 12 - Create New Item
def create_TodoItem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todos/items/create.html", context)


# Feature 13 - Edit Item
def update_TodoItem(request, id):
    TodoList_list = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=TodoList_list)
        if form.is_valid():
            TodoList_list = form.save()
            return redirect("todo_list_detail", id=TodoList_list.id)

    else:
        form = TodoItemForm(instance=TodoList_list)

    context = {"form": form}

    return render(request, "todos/items/edit.html", context)
